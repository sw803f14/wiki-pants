# Wiki prototype for wukong db.
Below is an outline of how the database will come to function.

# P2P Wiki Prototype
The aim of the prototype implementation is to use the API in a scenario which is close to the real world and showing that the service is sound.

## Concept
Community wikis have gained a lot of traction, things like [MediaWiki][] and [DokuWiki][] have millions of users. However these wikis are not very portable, we aim to create a wiki application which can be used in a distributed P2P network leveraging many interesting features of that world.

[MediaWiki]: http://www.mediawiki.org/wiki/MediaWiki
[DokuWiki]: https://www.dokuwiki.org/dokuwiki

## Implementation


### Markup Language
Using the [Markdown][] flavour of markup language would probably make the service easy to use, especially since you can embed html in Markdown documents.

There are concerns about Markdown as markup language as it does not include ways of doing everything and embedded html is not necessarily secure.

[Markdown]: http://daringfireball.net/projects/markdown/

### Sharing
The implementation will, of course, use the P2P service for sharing.

A wiki object will contain some fields:

- `title` a human readable name which identifies the wiki.
- `subject` a human readable subject for the wiki, helps in identifying further if there are several wikis of the same name.
- `identifier` a unique string which ensures that wikis are not accidentally merged with wikis of the same name.

An article contains the following fields:

- `title` the identity of the article, this should be unique for a certain wiki.
- `body` the body of the article.
- `tags` tags identifying the article.
- `wiki` which wiki this article is associated with.
- `time_modified` when the article was modified.

Articles will be able to reference other articles by special syntax, not yet decided on.

Articles will be archived in a `ArticleArchive` datatype:

- `time_modified` inherited field from article identifying when this article was modified.
- `body` the body when the article was archived.

There will also be an media type, this is done to save the user some headaches if very large pieces of media like videos and HD images are uploaded.

The media type will contain:

- `name` name of the media object.
- `size` to identify if a piece of media should be downloaded.
