package test;

import android.test.AndroidTestCase;
import android.test.RenamingDelegatingContext;

/**
 * Created by mark on 4/21/14.
 */
public abstract class BasePantsTest extends AndroidTestCase{
    public static final String TEST_FILE_PREFIX = "test_";
    protected RenamingDelegatingContext context;

    protected void setContext(RenamingDelegatingContext context){
        this.context = context;
    }

    @Override
    public void setUp() throws Exception {
        // This is basically needed by all of the DB tests,
        // hope it isn't expensive.
        setContext(new RenamingDelegatingContext(getContext(),
                TEST_FILE_PREFIX));
    }
}
