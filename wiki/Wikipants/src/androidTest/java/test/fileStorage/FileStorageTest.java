package test.fileStorage;

import android.os.Environment;
import android.test.AndroidTestCase;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringBufferInputStream;

import dk.wukong.wikipants.wikipants.util.ArticleFileManager;
import test.BasePantsTest;

/**
 * Created by mark on 5/19/14.
 */
public class FileStorageTest extends BasePantsTest {
    @Override
    public void tearDown() throws Exception{

    }

    public void testArticleFileManagerCreate(){
        File file;
        ArticleFileManager afm = new ArticleFileManager(context);
        long id, wiki;
        String body, type;
        BufferedReader reader;

        id = 1;
        wiki = 1;
        body = "body";
        type = "test";

        file = afm.create(id,wiki,body,type);

        try{
            reader = new BufferedReader(new FileReader(file));
            reader.ready();
            assertEquals(body, reader.readLine());
        } catch (IOException e){
            String msg;
            msg = "Could not read file %s\n%s";
            msg = String.format(msg, file.getName(), e.getMessage());
            fail(msg);
        } finally {
            file.delete();
        }

    }

    /**
     * Testing how file read/write works out.
     */
    public void testFileReadWrite(){
        File file;
        File docs =
                context.getExternalFilesDir("markdown");
        docs.mkdir();
        docs.isDirectory();
        String hello = "Hello World";
        BufferedWriter writer;
        BufferedReader reader;

        file = new File(docs + File.separator + "test.markdown");
        file.exists();
        try {
            if (file.createNewFile()) {
                Log.d("yay", "created a file?");
            }

            writer = new BufferedWriter(new FileWriter(file));
            writer.write(hello);
            writer.newLine();
            writer.flush();
            writer.close();

            reader = new BufferedReader(new FileReader(file));
            reader.ready();
            assertEquals(hello, reader.readLine());
            reader.close();
        } catch (IOException e){
            fail("Could not write.\n" + e.getMessage());
        } finally {
            file.delete();
        }
    }

    public void testFilePlace(){

    }
}
