package test.dataSourceTests.wikiTests;

import dk.wukong.wikipants.wikipants.database.dao.WikiDataSource;
import dk.wukong.wikipants.wikipants.models.Wiki;
import test.dataSourceTests.DAOBaseTest;

/**
 * Created by mark on 4/28/14.
 *
 */
public abstract class BaseWikiDSTest
        extends DAOBaseTest<Wiki, WikiDataSource> {
    static String title = "title";
    static String subject = "subject";
    int count = 0;
    Wiki wikiOne, wikiTwo;

    @Override
    public void setUp() throws Exception{
        super.setUp();
        wikiOne = createObject();
        wikiTwo = createObject();
    }

    @Override
    protected WikiDataSource createSource() {
        return new WikiDataSource(context);
    }

    @Override
    protected Wiki createObject() {
        Wiki wiki;

        if(count == 0){
            wiki = new Wiki(title, subject);
        } else {
            wiki = new Wiki(title+count, subject+count);
        }
        count += 1;

        return wiki;
    }


}
