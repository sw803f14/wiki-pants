package test.dataSourceTests.wikiTests;

import java.sql.SQLException;

import dk.wukong.wikipants.wikipants.models.Wiki;

/**
 * Created by mark on 4/21/14.
 *
 */
public class DSWikiDBTest extends BaseWikiDSTest {
    @Override
    public void setUp() throws Exception {
        super.setUp();
        dataSource.open();
        // a wee bit redundant.
        wikiTwo = dataSource.add(wikiOne);
    }

    @Override
    public void tearDown() {
        dataSource.close();
    }

    public void testInsertWiki() throws Exception {
        Wiki addedWiki;
        String failMsg = String.format("No id was added to Wiki in %s",
                dataSource.getClass().getSimpleName());

        assertNull("An id was added to Wiki in initialisation!",
                wikiOne.getId());

        addedWiki = dataSource.add(wikiOne);

        assertNotNull(failMsg,
                addedWiki.getId());
    }

    public void testAccessWikiByID() throws SQLException {
        Wiki gotWiki;
        String msg;

        msg = "The wiki got from the DB is not the same as the one created.";
        gotWiki = dataSource.get(wikiTwo.getId());

        assertTrue(msg, wikiTwo.equals(gotWiki));
    }

    public void testNotChangingIdentifierMakesEqual() throws SQLException {
        Wiki newWiki;
        String msg;
        msg = "A Wiki added under the same identifier as another is " +
                "not regarded as the same.";

        newWiki = new Wiki(
                "New Title",
                "New Subject",
                null
        );
        newWiki.setIdentifier(wikiTwo.getIdentifier());
        newWiki = dataSource.add(newWiki);

        assertTrue(msg, newWiki.equals(wikiTwo));
    }

    public void testIdentifierAndID() throws SQLException {
        Wiki newWiki;
        String msg;

        msg = "A Wiki added under same identifier and id as another is not " +
                "added to the database.";

        newWiki = new Wiki(
                "New Title",
                "New Subject",
                wikiTwo.getId()
        );

        newWiki = dataSource.update(newWiki);

        assertEquals(msg, newWiki.getId(), wikiTwo.getId());
    }


}