package test.dataSourceTests.wikiTests;

import android.content.ContentValues;

import dk.wukong.wikipants.wikipants.database.contract.WikiEntry;
import dk.wukong.wikipants.wikipants.database.dao.WikiDataSource;
import dk.wukong.wikipants.wikipants.models.Wiki;

/**
 * Created by mark on 4/18/14.
 *
 */
public class DSWikiTest extends BaseWikiDSTest {
    private static final String TAG = "TEST_WIKIDATASOURCE";

    private static String title = "title";
    private static String subject = "subject";
    private int count = 0;

    @Override
    public void setUp() throws Exception{
        super.setUp();
    }

    @Override
    protected WikiDataSource createSource(){
        return new WikiDataSource(context);
    }

    @Override
    protected Wiki createObject() {
        Wiki wiki;

        if(count == 0){
            wiki = new Wiki(title, subject);
        } else {
            wiki = new Wiki(title+count, subject+count);
        }
        count++;

        return wiki;
    }

    public void testWikiToContentValues(){
        ContentValues contentValues;
        ContentValues testContentValues = new ContentValues();

        testContentValues.put(WikiEntry.COLUMN_NAME_TITLE, title);
        testContentValues.put(WikiEntry.COLUMN_NAME_SUBJECT, subject);
        testContentValues.put(WikiEntry.COLUMN_NAME_IDENT,
                Wiki.generateIdent(title, subject));
        testContentValues.put(WikiEntry._ID, (Long) null);

        contentValues = dataSource.wikiToValues(wikiOne);

        assertEquals("The created ContentValues do not match the test.",
                testContentValues, contentValues);
    }
}
