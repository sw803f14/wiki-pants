package test.dataSourceTests.articleTests;

import android.content.ContentValues;

import dk.wukong.wikipants.wikipants.database.contract.ArticleEntry;
import dk.wukong.wikipants.wikipants.models.Article;

/**
 * Created by mark on 4/30/14.
 *
 */
public class DSArticleTest extends BaseArticleDSTest {
    ContentValues test;

    @Override
    public void setUp() throws Exception{
        super.setUp();
        test = new ContentValues();

        test.put(ArticleEntry._ID, articleOne.getId());
        test.put(ArticleEntry.COLUMN_NAME_TITLE, articleOne.getTitle());
        test.put(ArticleEntry.COLUMN_NAME_BODY, articleOne.getBody());
        test.put(ArticleEntry.COLUMN_NAME_MODIFIED, articleOne.getModified());

    }

    public void testContenValuesConversion(){
        assertEquals(
                "Incorrectly formatted ContentValues returned by converter.",
                test,
                dataSource.articleToValues(articleOne)
                );
    }


}
