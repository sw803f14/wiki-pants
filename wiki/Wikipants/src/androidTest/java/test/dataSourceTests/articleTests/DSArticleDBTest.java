package test.dataSourceTests.articleTests;

import java.util.ArrayList;
import java.util.Iterator;

import dk.wukong.wikipants.wikipants.database.dao.WikiDataSource;
import dk.wukong.wikipants.wikipants.models.Article;
import dk.wukong.wikipants.wikipants.models.Wiki;

/**
 * Created by mark on 4/30/14.
 *
 */
public class DSArticleDBTest extends BaseArticleDSTest {
    Article addedArticle;
    Wiki wiki;
    WikiDataSource wikiDataSource;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        wikiDataSource = new WikiDataSource(context);
        dataSource.open();
        wikiDataSource.open();

        wiki = wikiDataSource.add(new Wiki("title", "subject"));
        articleOne.setWiki(wiki);
        addedArticle = dataSource.add(articleOne);
    }

    @Override
    public void tearDown() throws Exception{
        dataSource.close();
    }

    public void testGetWikiTitle() throws Exception {
        String newWikiTitle;
        Wiki nWiki;
        Article nArticle, gotArticle;

        newWikiTitle = "new title";
        nWiki = wikiDataSource.add(new Wiki(newWikiTitle, " "));
        // add an article exactly like articleOne to the database.
        nArticle = articleOne;
        nArticle.setWiki(nWiki);
        nArticle = dataSource.add(nArticle);

        gotArticle = dataSource.get(nWiki.getId(), title);

        String msg =
                "Article gotten via wiki id and title do not match their " +
                        "original";

        assertEquals(msg,
                gotArticle,
                nArticle);
    }

    public void testGetSingleByTag(){
        ArrayList<Article> articles = dataSource.getArticleListByTag(tags[0]);
        if(articles.size() != 1){
            fail("Incorrect number of articles returned from known tag search");
        } else {
            assertEquals(articles.get(0), addedArticle);
        }
    }

    public void testGetArticleById() throws Exception {
        dataSource.open();
        addedArticle = dataSource.get(addedArticle.getId());

    }

    public void testGetMultipleByTag() throws Exception {
        articleTwo.setWiki(wiki);
        Article addedArticleTwo = dataSource.add(articleTwo);
        ArrayList<Article> addedArticles = new ArrayList<Article>();
        ArrayList<Article> articles =
                dataSource.getArticleListByTags(tags);
        addedArticles.add(addedArticle);
        addedArticles.add(addedArticleTwo);
        auxForTagSearch(articles, addedArticles);
    }

    public void auxForTagSearch(ArrayList<Article> articles,
                                ArrayList<Article> addedArticles){
        if(articles.size() != addedArticles.size()){
            String failMsg =
                    "Wrong number of elements returned by tag search.\n" +
                    "Was: %d\n" +
                    "Should at least be: %d";
            fail(String.format(failMsg, articles.size(), addedArticles.size()));
        } else {
            Iterator<Article> iteratorA = articles.iterator();
            Iterator<Article> iteratorB = addedArticles.iterator();

            // which iterator does not really matter.
            while(iteratorA.hasNext() || iteratorB.hasNext()){
                assertTrue(
                        "Tag search returned an incorrect article.",
                        iteratorA.next().equals(iteratorB.next())
                );
            }
        }
    }

    public void testGetGetsArticle() throws Exception{
        Article test = dataSource.get(addedArticle.getId());
        assertEquals(test, addedArticle);
    }
}
