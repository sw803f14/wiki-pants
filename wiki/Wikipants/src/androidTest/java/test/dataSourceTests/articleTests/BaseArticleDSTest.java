package test.dataSourceTests.articleTests;

import dk.wukong.wikipants.wikipants.database.dao.ArticleDataSource;
import dk.wukong.wikipants.wikipants.models.Article;
import test.dataSourceTests.DAOBaseTest;

/**
 * Created by mark on 4/30/14.
 *
 */
public abstract class BaseArticleDSTest
        extends DAOBaseTest<Article, ArticleDataSource> {
    String title = "title";
    String body = "body";
    String[] tags = {"tag1", "tag2", "tag3"};
    long modified = 0;
    Article articleOne, articleTwo;

    int count = 0;

    @Override
    public void setUp() throws Exception{
        super.setUp();
        articleOne = createObject();
        articleTwo = createObject();
    }

    @Override
    protected ArticleDataSource createSource() {
        return new ArticleDataSource(context);
    }

    @Override
    protected Article createObject() {
        Article article;
        if(count == 0) {
            article = new Article(title, body, modified, tags);
        } else {
            article = new Article(title+count, body, modified, tags);
        }
        count += 1;
        return article;
    }
}
