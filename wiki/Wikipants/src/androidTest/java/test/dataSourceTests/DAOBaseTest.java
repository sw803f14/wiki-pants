package test.dataSourceTests;

import android.content.ContentValues;

import dk.wukong.wikipants.wikipants.database.dao.BasePantsDataSource;
import test.BasePantsTest;

/**
 * Created by mark on 4/24/14.
 *
 */
public abstract class DAOBaseTest
        <Type, TypeSource extends BasePantsDataSource>
        extends BasePantsTest {

    protected TypeSource dataSource;

    protected void setDataSource(TypeSource dataSource){
        this.dataSource = dataSource;
    }
    
    @Override
    public void setUp() throws Exception {
        super.setUp();
        setDataSource(createSource());
    }

    protected abstract TypeSource createSource();
    protected abstract Type createObject();

    public void testMetaCreateBehavesCorrectly() {
        Type objectOne, objectTwo;
        String message;

        objectOne = createObject();
        objectTwo = createObject();

        message = "The two %s objects created by createObject are the same.";
        message = String.format(message,
                objectOne.getClass().getSimpleName());

        assertFalse(message, objectOne.equals(objectTwo));
    }
}
