package test.helperTests;

import android.database.sqlite.SQLiteDatabase;

import dk.wukong.wikipants.wikipants.database.contract.ArticleEntry;
import dk.wukong.wikipants.wikipants.database.helper.PantsDBHelper;
import test.BasePantsTest;

/**
 * Created by mark on 4/18/14.
 *
 */
public class PantsDBHelperTest
        extends BasePantsTest{
    protected static final String TAG = "BaseHelperTest";

    protected PantsDBHelper dbHelper;

    protected PantsDBHelper getDBHelper(){
        return this.dbHelper;
    }

    public void setUp() throws Exception {
        super.setUp();
        dbHelper = new PantsDBHelper(context);
    }

    public void testHelperCanGetReadableDatabase() throws Exception{
        SQLiteDatabase db = getDBHelper().getReadableDatabase();

        assertTrue("Could not open Database, check your SQL", db.isOpen());
    }

    public void testHelperCanGetWritableDatabase() throws Exception{
        SQLiteDatabase db = getDBHelper().getWritableDatabase();

        assertTrue("Could not open Database, check your SQL", db.isOpen());
    }
}
