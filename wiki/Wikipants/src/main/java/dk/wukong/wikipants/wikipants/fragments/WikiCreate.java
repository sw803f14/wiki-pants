package dk.wukong.wikipants.wikipants.fragments;



import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import java.sql.SQLException;

import dk.wukong.wikipants.wikipants.R;
import dk.wukong.wikipants.wikipants.activities.MainActivity;
import dk.wukong.wikipants.wikipants.database.dao.ArticleDataSource;
import dk.wukong.wikipants.wikipants.database.dao.WikiDataSource;
import dk.wukong.wikipants.wikipants.models.Article;
import dk.wukong.wikipants.wikipants.models.Wiki;

/**
 * A simple {@link Fragment} subclass.
 *
 */
public class WikiCreate extends Fragment {
    private static final String TAG = WikiCreate.class.getSimpleName();

    WikiCreateCallback callback;

    public WikiCreate() {
        // Required empty public constructor
    }

    public long submit(){
        View view;
        WikiDataSource dataSource;
        Wiki wiki;
        String title, subject;

        view = getView();

        title = ((TextView) view.findViewById(R.id.title))
                .getText().toString();

        subject = ((TextView) view.findViewById(R.id.subject))
                .getText().toString();

        wiki = new Wiki(title, subject);
        dataSource = new WikiDataSource(getActivity());
        try {
            dataSource.open();
            wiki = dataSource.add(wiki);
        } catch (SQLException e) {
            Log.e(TAG, "Could not open database connection.");
        } finally {
            dataSource.close();
        }
        return wiki.getId();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_wiki_create,
                container,
                false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_submit){
            long wikiId = submit();
            callback.viewWiki(wikiId, false);
            return true;
        }

        return true;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callback = (WikiCreateCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement WikiCreateCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public interface WikiCreateCallback {
        public void viewWiki(long id, boolean backup);
    }

}
