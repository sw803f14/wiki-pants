package dk.wukong.wikipants.wikipants.fragments;

import android.app.Activity;
import android.app.ListFragment;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.sql.SQLException;

import dk.wukong.wikipants.wikipants.R;
import dk.wukong.wikipants.wikipants.database.contract.WikiEntry;
import dk.wukong.wikipants.wikipants.database.dao.WikiDataSource;
import dk.wukong.wikipants.wikipants.views.WikiCursorAdapter;

/**
 * Created by mark on 5/16/14.
 */
public class WikiList extends ListFragment {
    private static final String TAG = WikiList.class.getSimpleName();

    private WikiListCallback callback;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_create){
            callback.createWiki(true);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        populateWikiList();
    }

    private void populateWikiList(){
        Cursor cursor;
        WikiDataSource wikiDataSource;
        WikiCursorAdapter adapter;

        wikiDataSource = new WikiDataSource(getActivity());
        try{
            wikiDataSource.open();
        } catch (SQLException e){
            Log.e(TAG, "Was unable to open database connection.");
        } finally {
            cursor = wikiDataSource.getLocalWikiCursor();
            adapter = new WikiCursorAdapter(getActivity(), cursor, 0);
            setListAdapter(adapter);

            wikiDataSource.close();
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        Cursor cursor = ((Cursor)l.getAdapter().getItem(position));
        callback.viewWiki(
                cursor.getLong(cursor.getColumnIndex(WikiEntry._ID)), true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callback = (WikiListCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnSelectItemListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }


    public interface WikiListCallback {
        public void viewWiki(long id, boolean backup);
        public void createWiki(boolean backup);
    }
}
