package dk.wukong.wikipants.wikipants.database.contract;

/**
 * Created by mark on 4/22/14.
 *
 */
public class WikiEntry extends BasePantsTable {
    public static String TABLE_NAME = "wiki";
    public static String COLUMN_NAME_TITLE = "title";
    public static String COLUMN_NAME_SUBJECT = "subject";
    public static String COLUMN_NAME_IDENT = "identifier";

    private static String[] COLUMNS = {
            _ID,
            COLUMN_NAME_TITLE,
            COLUMN_NAME_SUBJECT,
            COLUMN_NAME_IDENT,
    };
    private static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
            COLUMN_NAME_TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            COLUMN_NAME_SUBJECT + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            COLUMN_NAME_IDENT + TEXT_TYPE +
            ");";

    @Override
    public String getCreateTable() {
        return CREATE_TABLE;
    }

    @Override
    public String getDeleteTable() {
        return DELETE_TABLE;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    public String getPrimaryKey() {
        return PRIMARY_KEY;
    }
}
