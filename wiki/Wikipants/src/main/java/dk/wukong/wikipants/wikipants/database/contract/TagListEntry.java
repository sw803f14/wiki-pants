package dk.wukong.wikipants.wikipants.database.contract;

/**
 * Created by mark on 4/22/14.
 */
public class TagListEntry extends BasePantsTable {
    public static String TABLE_NAME = "tag_list";
    public static String COLUMN_NAME_TAG = "tag";
    public static String COLUMN_NAME_ARTICLE = "article";

    private static String[] COLUMNS = {
            _ID,
            COLUMN_NAME_TAG,
            COLUMN_NAME_ARTICLE
    };
    private static String TAG_FOREIGN =
            String.format(FOREIGN_KEY_RELATION,
                    COLUMN_NAME_TAG,
                    TagEntry.TABLE_NAME,
                    TagEntry.PRIMARY_KEY) +
                    ON_DELETE_CASCADE + ON_UPDATE_CASCADE;
    private static String ARTICLE_FOREIGN =
            String.format(FOREIGN_KEY_RELATION,
                    COLUMN_NAME_ARTICLE,
                    ArticleEntry.TABLE_NAME,
                    ArticleEntry.PRIMARY_KEY) +
                    ON_DELETE_CASCADE + ON_UPDATE_CASCADE;
    private static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
            COLUMN_NAME_TAG + NOT_NULL + COMMA_SEP +
            COLUMN_NAME_ARTICLE + NOT_NULL + COMMA_SEP +
            TAG_FOREIGN + COMMA_SEP +
            ARTICLE_FOREIGN +
            ");";

    @Override
    public String getCreateTable() {
        return CREATE_TABLE;
    }

    @Override
    public String getDeleteTable() {
        return DELETE_TABLE;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    public String getPrimaryKey() {
        return PRIMARY_KEY;
    }
}
