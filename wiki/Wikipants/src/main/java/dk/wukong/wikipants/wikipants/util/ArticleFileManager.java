package dk.wukong.wikipants.wikipants.util;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by mark on 5/20/14.
 */
public class ArticleFileManager {
    private static final String TAG = ArticleFileManager.class.getSimpleName();
    File location;

    public ArticleFileManager(Context context) {
        location = context.getExternalFilesDir("wiki");
        if(!location.exists()){
            location.mkdirs();
        }
    }

    public File get(long wiki, long article){
        return get(wiki, article, "markdown");
    }

    public File get(long wiki, long article, String type){
        String dir, fileLocation;
        File file;

        dir = String.format("%d/%d/", wiki,article);
        file = new File(location, dir);
        if(!file.exists()) {
            file.mkdirs();
        }

        fileLocation = String.format("%d/%d/%s", wiki,article,type);
        file = new File(location, fileLocation);

        return file;
    }

    public File create(long wiki, long id, String body){
        // md is the primary file type.
        return create(wiki,id,body,"markdown");
    }

    public File create(long wiki, long id, String body, String type){
        File file;

        file = get(wiki, id, type);
        overWriteFile(file,body);

        return file;
    }

    public File update(long wiki, long id, String body){
        // md is the primary file type.
        return update(wiki,id,body,"markdown");
    }

    public File update(long wiki, long id, String body, String type) {
        // the actions performed in update are the same as create.
        return create(wiki,id,body,type);
    }

    public void overWriteFile(File file, String with){
        BufferedOutputStream writer;
        if(file.exists()) {
            file.delete();
        }

        try {
            writer = new BufferedOutputStream(new FileOutputStream(file));
            writer.write(with.getBytes());
            writer.close();
        } catch (IOException e){
            Log.e(TAG, e.getMessage());
        }
    }
}
