package dk.wukong.wikipants.wikipants.views;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import dk.wukong.wikipants.wikipants.R;
import dk.wukong.wikipants.wikipants.models.Wiki;

/**
* Created by mark on 5/16/14.
*/
public class ArticleArrayAdapter extends ArrayAdapter<Wiki> {
    Activity context;
    List<Wiki> values;

    static class ViewHolder{
        public TextView title;
        public TextView subject;
    }

    public ArticleArrayAdapter(Activity context, int textViewResourceId,
                               List<Wiki> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.values = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Wiki wiki = values.get(position);
        View rowView = convertView;

        if(rowView == null){
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.wiki_row_layout, null);

            ViewHolder viewHolder = new ViewHolder();
            viewHolder.title = (TextView)
                    rowView.findViewById(R.id.wikiRowTitle);

            viewHolder.subject = (TextView)
                    rowView.findViewById(R.id.wikiRowSubject);
            rowView.setTag(viewHolder);
        }
        ViewHolder holder = (ViewHolder) rowView.getTag();

        holder.title.setText(wiki.getTitle());
        holder.subject.setText(wiki.getSubject());

        return rowView;
    }
}
