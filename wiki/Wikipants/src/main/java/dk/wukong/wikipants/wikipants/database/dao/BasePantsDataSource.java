package dk.wukong.wikipants.wikipants.database.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteClosable;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.Closeable;
import java.sql.SQLException;

import dk.wukong.wikipants.wikipants.database.contract.BasePantsTable;
import dk.wukong.wikipants.wikipants.database.helper.PantsDBHelper;

/**
 * Created by mark on 4/17/14.
 *
 */
public abstract class BasePantsDataSource <Table extends BasePantsTable> {
    private static String TAG = "BasePantsDataSource";

    protected SQLiteDatabase database;
    protected PantsDBHelper dbHelper;
    Table table;

    public BasePantsDataSource(Table table){
        this.table = table;
    }

    public boolean remove(Object object){
        return false;
    }

    public abstract ContentValues objectToValues(Object object);
    public abstract Object cursorToObject(Cursor cursor);

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public SQLiteDatabase getDatabase(){
        return database;
    }

    public PantsDBHelper getDBHelper(){
        return dbHelper;
    }

    public Object get(long id) throws SQLException {
        Object object;
        Cursor cursor;

        cursor = getData(id);
        cursor.moveToFirst();
        object = cursorToObject(cursor);
        cursor.close();

        return object;
    }

    public Object add(Object object) throws SQLException{
        ContentValues vcs;
        Cursor cursor;

        vcs = objectToValues(object);

        cursor = addData(vcs);
        cursor.moveToFirst();

        object = cursorToObject(cursor);

        cursor.close();
        return object;
    }

    public Object update(Object object){
        ContentValues vcs;
        Cursor cursor;

        vcs = objectToValues(object);

        cursor = updateData(vcs);
        cursor.moveToFirst();

        object = cursorToObject(cursor);

        cursor.close();
        return object;
    }

    /**
     * Will get a cursor to a specific id of whatever table.TABLE_NAME
     * is.
     *
     * This method should be used only in cases where a getter for the
     * specific piece of data is not available.
     * @param id an id of a row in table.TABLE_NAME.
     * @return A cursor to a database object in table.TABLE_NAME
     * @throws NullPointerException If the database was not opened a NPE will
     * be thrown.
     */
    public Cursor getData(Long id) throws NullPointerException{
        Cursor cursor;

        String query = String.format(
                "%s = ?",
                table.getPrimaryKey()
        );

        String[] argumentArray = new String[]{
                Long.toString(id),
        };

        try {
            cursor = database.query(
                    table.getTableName(),
                    table.getColumns(),
                    query,
                    argumentArray,
                    null,
                    null,
                    null
            );
        } catch (NullPointerException e){
            String msg =
                    "A NPE was thrown, maybe you forgot to open " +
                            "the database?";
            Log.e(TAG, msg);
            // propagating the npe up the stack.
            throw e;
        }

        return cursor;
    }

    /**
     * Adds a piece of data to table.TABLE_NAME, the data should be formatted
     * to match the actual layout of the table.
     * @param data data contained within a table of table.TABLE_NAME
     * @return a Cursor to the database object created.
     * @throws NullPointerException If the database was not opened a NPE will
     * be thrown.
     */
    public Cursor addData(ContentValues data)
            throws NullPointerException, SQLException {
        Cursor cursor;

        if(database != null){
            long id = database.insertOrThrow(table.getTableName(),
                    null,
                    data);
            cursor = getData(id);
        } else {
            String msg =
                    "A NPE was thrown, maybe you forgot to open " +
                            "the database?";
            Log.e(TAG, msg);
            // throwing a NPE up the stack to avoid confusion.
            throw new NullPointerException("Database was not opened.");
        }

        return cursor;
    }

    public Cursor updateData(ContentValues data) throws NullPointerException{
        Cursor cursor;

        if(database != null){
            String primaryKey = table.getPrimaryKey();
            database.update(
                    table.getTableName(),
                    data,
                    String.format("%s=?", primaryKey),
                    new String[]{Long.toString(data.getAsLong(primaryKey))});
            cursor = getData(data.getAsLong(primaryKey));
        } else {
            String msg =
                    "A NPE was thrown, maybe you forgot to open " +
                            "the database?";
            Log.e(TAG, msg);
            // throwing a NPE up the stack to avoid confusion.
            throw new NullPointerException("Database was not opened.");
        }

        return cursor;
    }

    public Cursor getCursor(String table,
                            String query,
                            String[] columns,
                            String[] argumentArray){
        Cursor cursor;

        try {
            cursor = database.query(
                    table,
                    columns,
                    query,
                    argumentArray,
                    null,
                    null,
                    null
            );
        } catch (NullPointerException e){
            String msg =
                    "A NPE was thrown, maybe you forgot to open" +
                            " the database?";
            Log.e(TAG, msg);
            // propagating the npe up the stack.
            throw e;
        }

        return cursor;
    }

}
