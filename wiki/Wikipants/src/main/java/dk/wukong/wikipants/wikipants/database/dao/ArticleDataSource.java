package dk.wukong.wikipants.wikipants.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteStatement;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import dk.wukong.wikipants.wikipants.database.contract.ArchiveArticleEntry;
import dk.wukong.wikipants.wikipants.database.contract.ArticleEntry;
import dk.wukong.wikipants.wikipants.database.contract.TagEntry;
import dk.wukong.wikipants.wikipants.database.contract.TagListEntry;
import dk.wukong.wikipants.wikipants.database.contract.WikiEntry;
import dk.wukong.wikipants.wikipants.database.helper.PantsDBHelper;
import dk.wukong.wikipants.wikipants.models.Article;

/**
 * Created by mark on 4/22/14.
 *
 */
public class ArticleDataSource extends BasePantsDataSource<ArticleEntry> {
    protected static final String TAG = "ArticleDataSource";

    /**
     * Intermediat class for some database objects which are only used within
     * Article access.
     */
    public class Tag {
        private Long id;
        private String tag;

        public Tag(long id, String tag){
            this.id = id;
            this.tag = tag;
        }

        public Tag(Cursor cursor){
            int i = 0;
            if(cursor.moveToFirst()) {
                this.id = cursor.getLong(i);
                this.tag = cursor.getString(++i);
            } else {
                this.id = null;
                this.tag = null;
            }

        }

        public ContentValues getValues(){
            ContentValues values = new ContentValues();

            values.put(TagEntry._ID, this.id);
            values.put(TagEntry.COLUMN_NAME_TAG, this.tag);

            return values;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getTag() {
            return tag;
        }

        public void setTag(String tag) {
            this.tag = tag;
        }
    }

    public class ArchiveArticle {
        private Long id;
        private String body;
        private long modified;
        private Long article;

        public ArchiveArticle(Long id,
                               String body,
                               long modified,
                               long article) {
            this.id = id;
            this.body = body;
            this.modified = modified;
            this.article = article;
        }

        public ArchiveArticle(Cursor cursor){
            int i = 0;
            if(cursor.moveToFirst()) {
                this.id = cursor.getLong(i);
                this.body = cursor.getString(++i);
                this.modified = cursor.getLong(++i);
                this.article = cursor.getLong(++i);
            } else {
                this.id = null;
                this.body = null;
                this.modified = 0;
                this.article = null;
            }

        }

        public ContentValues getValues(){
            ContentValues values = new ContentValues();

            values.put(ArchiveArticleEntry._ID, this.id);
            values.put(ArchiveArticleEntry.COLUMN_NAME_BODY, this.body);
            values.put(ArchiveArticleEntry.COLUMN_NAME_MODIFIED, this.modified);
            values.put(ArchiveArticleEntry.COLUMN_NAME_ARTICLE, this.article);

            return values;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }

        public long getModified() {
            return modified;
        }

        public void setModified(long modified) {
            this.modified = modified;
        }

        public long getArticle() {
            return article;
        }

        public void setArticle(long article) {
            this.article = article;
        }
    }

    public class TagRelation {
        Long id;
        Long article;
        Long tag;

        public TagRelation(Long id, long article, long tag) {
            this.id = id;
            this.article = article;
            this.tag = tag;
        }

        public TagRelation(Cursor cursor){
            int i = 0;

            if(cursor.moveToFirst()) {
                this.id = cursor.getLong(i);
                this.article = cursor.getLong(++i);
                this.tag = cursor.getLong(++i);
            } else {
                this.id = null;
                this.article = null;
                this.tag = null;
            }

        }

        public ContentValues getValues(){
            ContentValues values = new ContentValues();
            TagListEntry entry = new TagListEntry();

            values.put(TagListEntry._ID, this.id);
            values.put(TagListEntry.COLUMN_NAME_ARTICLE, this.article);
            values.put(TagListEntry.COLUMN_NAME_TAG, this.tag);

            return values;
        }

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public long getArticle() {
            return article;
        }

        public void setArticle(long article) {
            this.article = article;
        }

        public long getTag() {
            return tag;
        }

        public void setTag(long tag) {
            this.tag = tag;
        }
    }

    public ArticleDataSource(Context context){
        super(new ArticleEntry());
        dbHelper = new PantsDBHelper(context);
    }

    public Tag getTagByTag(String tag){
        Tag tagValue;
        TagEntry tagEntry = new TagEntry();
        Cursor cursor = getCursor(
                TagEntry.TABLE_NAME,
                "tag = ?",
                tagEntry.getColumns(),
                new String[]{tag}
        );

        tagValue = new Tag(cursor);

        return tagValue;
    }

    public Tag getTagListByTagLimitToWiki(long wikiId, String tag){
        Tag tagValue;
        TagEntry tagEntry = new TagEntry();

        Cursor cursor = getCursor(
                TagEntry.TABLE_NAME,
                String.format("tag = ? AND wiki = %d",wikiId) ,
                tagEntry.getColumns(),
                new String[]{tag}
        );

        tagValue = new Tag(cursor);

        return tagValue;
    }

    public ArrayList<Article> getArticleListByRowValue(String row,
                                                       String value){
        String table = this.table.getTableName();
        String query;
        String[] columns;
        String[] argumentArray;
        Cursor cursor;
        ArrayList<Article> articleList = new ArrayList<Article>();

        query = String.format(
                "%s = ?",
                row
        );

        columns = this.table.getColumns();

        argumentArray = new String[]{
                value,
        };

        cursor = getCursor(table, query, columns, argumentArray);

        if(cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                articleList.add(cursorToObject(cursor));
                cursor.moveToNext();
            }
        }
        return articleList;

    }

    public ArrayList<Article> getArticleListByWiki(long id){
        return getArticleListByRowValue(
                ArticleEntry.COLUMN_NAME_WIKI,
                Long.toString(id)
        );
    }


    public ArrayList<Article> getArticleListByTags(Collection<String> tags){
        return null;
    }

    public ArrayList<Article> getArticleListByTags(String[] tags) {
        String query;
        String join;
        String and;
        String[] selection;
        Cursor cursor;
        ArrayList<Article> articles;

        join = String.format(
                "%1$s JOIN %3$s ON %1$s.%2$s=%3$s.%4$s",
                TagListEntry.TABLE_NAME,
                TagListEntry.COLUMN_NAME_ARTICLE,
                ArticleEntry.TABLE_NAME,
                ArticleEntry._ID
        );

        join = String.format(
                "%1$s JOIN %4$s ON %2$s.%3$s=%4$s.%5$s",
                join,
                TagListEntry.TABLE_NAME,
                TagListEntry.COLUMN_NAME_TAG,
                TagEntry.TABLE_NAME,
                TagEntry._ID
        );

        // strange, should work..
        and = "(";
        int i = 0;
        while(i < tags.length){
            ++i;
            and += String.format(
                    "%s.%s=?",
                    TagEntry.TABLE_NAME,
                    TagEntry.COLUMN_NAME_TAG
            );

            if(i != tags.length){
                and += " OR ";
            }

        }
        and += ")";

        query = "SELECT DISTINCT %s FROM %s AND %s;";
        query = String.format(query,
                ArticleEntry.TABLE_NAME + ".*",
                join,
                and
        );

        selection = tags;

        cursor = database.rawQuery(query, selection);
        articles = new ArrayList<Article>();
        if(cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                articles.add(cursorToObject(cursor));
                cursor.moveToNext();
            }
        }

        return articles;
    }

    public ArrayList<Article> getArticleListByTag(String tag){
        String query;
        String join;
        String and;
        String[] selection;
        Cursor cursor;
        ArrayList<Article> articles;

        join = String.format(
                "%1$s JOIN %3$s ON %1$s.%2$s=%3$s.%4$s",
                TagListEntry.TABLE_NAME,
                TagListEntry.COLUMN_NAME_ARTICLE,
                ArticleEntry.TABLE_NAME,
                ArticleEntry._ID
                );

        join = String.format(
                "%1$s JOIN %4$s ON %2$s.%3$s=%4$s.%5$s",
                join,
                TagListEntry.TABLE_NAME,
                TagListEntry.COLUMN_NAME_TAG,
                TagEntry.TABLE_NAME,
                TagEntry._ID
        );

        and = String.format(
                "%s.%s=?",
                TagEntry.TABLE_NAME,
                TagEntry.COLUMN_NAME_TAG
        );

        query = "SELECT DISTINCT %s FROM %s AND %s;";
        query = String.format(query,
                ArticleEntry.TABLE_NAME + ".*",
                join,
                and
                );
        selection = new String[]{tag};

        cursor = database.rawQuery(query, selection);
        articles = new ArrayList<Article>();

        if(cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                articles.add(cursorToObject(cursor));
                cursor.moveToNext();
            }
        }

        return articles;
    }

    @Override
    public Article get(long id) throws SQLException{
        Article article = (Article) super.get(id);

        return article;
    }

    public ArrayList<String> getTagListByArticle(long id){
        Cursor cursor;
        String join, query, and;
        ArrayList<String> tags;
        SQLiteStatement statement;
        // Example query:
        // SELECT tag.tag FROM tag_list
        // JOIN tag ON tag._id=tag_list.tag
        // AND tag_list.article=?
        query = "SELECT %1$s.%3$s FROM " +
                "%1$s JOIN %2$s ON %1$s.%4$s = %2$s.%1$s " +
                "AND %2$s.%5$s=%6$d";
        query =
                String.format(query,
                        TagEntry.TABLE_NAME,
                        TagListEntry.TABLE_NAME,
                        TagEntry.COLUMN_NAME_TAG,
                        TagEntry.PRIMARY_KEY,
                        TagListEntry.COLUMN_NAME_ARTICLE,
                        id
                        );



        tags = new ArrayList<String>();
        cursor = database.rawQuery(query, new String[]{});
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            tags.add(cursor.getString(0));
            cursor.moveToNext();
        }

        return tags;
    }

    public Article add(Article article) throws SQLException {
        Article added = (Article) super.add(article);
        added.setTags(article.getTags());
        connectTags(added);

        return added;
    }

    public ArrayList<TagRelation> connectTags(Article article){
        Long id = article.getId();
        ArrayList<Tag> addedTags = addOrUpdateTags(article.getTags());
        ArrayList<TagRelation> links = new ArrayList<TagRelation>();

        for(Tag tag: addedTags){
            links.add(createLink(id, tag.getId()));
        }

        return links;
    }

    public ArrayList<Article> getArticleListByTitle(String title){
        return getArticleListByRowValue(ArticleEntry.COLUMN_NAME_TITLE, title);
    }

    public long getMainArticleForWiki(long wiki){
        Cursor cursor;

        cursor = getArticleByTitleAndWiki("main",
                                          wiki,
                                          new String[]{ArticleEntry._ID});
        if(!cursor.moveToFirst()){
            return 0;
        } else {
            return cursor.getLong(0);
        }
    }

    public Article get(long wiki, String title){
        Cursor cursor;
        String query = "%1$s.%2$s = ? AND %1$s.%3$s = %4$d";
        cursor = getCursor(
                ArticleEntry.TABLE_NAME,
                String.format(query,
                        ArticleEntry.TABLE_NAME,
                        ArticleEntry.COLUMN_NAME_TITLE,
                        ArticleEntry.COLUMN_NAME_WIKI,
                        wiki
                ),
                null,
                new String[]{title}
        );

        if(cursor.moveToFirst()){
            return cursorToObject(cursor);
        } else {
            return null;
        }
    }

    public Article get(String wikiTitle, String articleTitle){
        Cursor cursor;
        // Example query:
        // SELECT article.* FROM
        // article JOIN wiki ON article.wiki=wiki._id
        // AND article.title=? AND wiki.title=?
        String query;
        String select = "SELECT %s.* FROM ";
        String join = "%1$s JOIN %3$s ON %1$s.%2$d=%3$s.%4$d ";

        String condition = "AND %s.%s=? AND %s.%s=?";

        select = String.format(select, ArticleEntry.TABLE_NAME);

        join = String.format(join,
                             ArticleEntry.TABLE_NAME,
                             ArticleEntry.COLUMN_NAME_WIKI,
                             WikiEntry.TABLE_NAME,
                             WikiEntry._ID);

        condition = String.format(condition,
                                  ArticleEntry.TABLE_NAME,
                                  ArticleEntry.COLUMN_NAME_TITLE,
                                  WikiEntry.TABLE_NAME,
                                  WikiEntry.COLUMN_NAME_TITLE);

        query = select + join + condition;

        cursor =
            database.rawQuery(query, new String[]{articleTitle, wikiTitle});

        if(cursor.moveToFirst()){
            return cursorToObject(cursor);
        } else {
            return null;
        }
    }

    public Cursor
    getArticleByTitleAndWiki(String title, long wiki, String[] columns){
        Cursor cursor;
        ArticleEntry entry = new ArticleEntry();
        String query = "%1$s.%2$s = ? AND %1$s.%3$s = %4$d";
        cursor = getCursor(
                ArticleEntry.TABLE_NAME,
                String.format(query,
                        ArticleEntry.TABLE_NAME,
                        ArticleEntry.COLUMN_NAME_TITLE,
                        ArticleEntry.COLUMN_NAME_WIKI,
                        wiki
                ),
                columns,
                new String[]{title}
        );

        return cursor;
    }

    public ContentValues getTRValues(Long id, long article, long tag){
        ContentValues values = new ContentValues();

        values.put(TagListEntry._ID, id);
        values.put(TagListEntry.COLUMN_NAME_ARTICLE, article);
        values.put(TagListEntry.COLUMN_NAME_TAG, tag);

        return values;
    }

    private TagRelation createLink(Long article, Long tag) {
        long id;
        Cursor cursor;
        TagListEntry tagListEntry = new TagListEntry();
        id = database.insert(
                TagListEntry.TABLE_NAME,
                null,
                getTRValues(null, article, tag)
        );

        if(id < 0){
            cursor = database.query(
                    TagListEntry.TABLE_NAME,
                    tagListEntry.getColumns(),
                    String.format("%1$s.%2$s=%4$d AND %1$s.%3$s=%5$d",
                            TagListEntry.TABLE_NAME,
                            TagListEntry.COLUMN_NAME_TAG,
                            TagListEntry.COLUMN_NAME_ARTICLE,
                            tag,
                            article),
                    new String[]{},
                    null,
                    null,
                    null
                    );
        } else {
            cursor = database.query(
                    TagListEntry.TABLE_NAME,
                    tagListEntry.getColumns(),
                    String.format("%1$s.%2$s=%3$d",
                            TagListEntry.TABLE_NAME,
                            TagListEntry._ID,
                            id
                            ),
                    new String[]{},
                    null,
                    null,
                    null
            );

        }

        return new TagRelation(cursor);
    }

    public ArrayList<Tag> addOrUpdateTags(Collection<String> tags){
        ArrayList<Tag> addedTags = new ArrayList<Tag>();

        for(String tag: tags){
            addedTags.add(addOrUpdateTag(tag));
        }

        return addedTags;
    }

    public Tag addOrUpdateTag(String tag){
        ContentValues cvs = new ContentValues();
        long id;
        TagEntry tagEntry = new TagEntry();
        Cursor cursor;
        Tag addedTag;
        cvs.put(TagEntry.COLUMN_NAME_TAG, tag);


        id = database.insert(
                TagEntry.TABLE_NAME,
                null,
                cvs
        );

        if(id < 0) {
            cursor = database.query(
                    TagEntry.TABLE_NAME,
                    tagEntry.getColumns(),
                    String.format("%1$s.%2$s=?",
                            TagEntry.TABLE_NAME,
                            TagEntry.COLUMN_NAME_TAG
                    ),
                    new String[]{tag},
                    null,
                    null,
                    null
            );
        } else {
            cursor = database.query(
                    TagEntry.TABLE_NAME,
                    tagEntry.getColumns(),
                    String.format("%1$s.%2$s=%3$d",
                            TagEntry.TABLE_NAME,
                            TagEntry._ID,
                            id
                    ),
                    new String[]{},
                    null,
                    null,
                    null
            );
        }

        addedTag = new Tag(cursor);

        return addedTag;
    }

    public Article update(Article article) {
        return (Article) super.update(article);
    }

    public boolean remove(Article article) {
        return false;
    }

    @Override
    public ContentValues objectToValues(Object article) {
        return articleToValues((Article) article);
    }

    public ContentValues articleToValues(Article article) {
        ContentValues contentValues = new ContentValues();
        Long id = article.getId();
        String title = article.getTitle();
        String body = article.getBody();
        Long wiki = article.getWiki();
        long modified = article.getModified();

        contentValues.put(ArticleEntry._ID, id);
        contentValues.put(ArticleEntry.COLUMN_NAME_TITLE, title);
        contentValues.put(ArticleEntry.COLUMN_NAME_BODY, body);

        contentValues.put(ArticleEntry.COLUMN_NAME_MODIFIED, modified);

        if(wiki != null) {
            contentValues.put(ArticleEntry.COLUMN_NAME_WIKI, wiki);
        }



        return contentValues;
    }

    @Override
    public Article cursorToObject(Cursor cursor) {
        Article article = new Article();
        int i = 0;
        article.setId(cursor.getLong(i));
        article.setTitle(cursor.getString(++i));
        article.setBody(cursor.getString(++i));
        article.setModified(cursor.getLong(++i));
        article.setWiki(cursor.getLong(++i));

        article.setTags(getTagListByArticle(article.getId()));

        return article;
    }
}
