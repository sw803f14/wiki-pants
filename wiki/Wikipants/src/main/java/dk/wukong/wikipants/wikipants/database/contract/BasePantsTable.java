package dk.wukong.wikipants.wikipants.database.contract;

import android.provider.BaseColumns;

/**
 * Created by mark on 4/22/14.
 *
 */
public abstract class BasePantsTable implements BaseColumns {
    private static String TAG = "PantsDBContract";

    protected static String TEXT_TYPE = " TEXT";
    protected static String INT_TYPE = " INTEGER";
    protected static String COMMA_SEP = ", ";
    protected static String NOT_NULL = " NOT NULL";
    protected static String UNIQUE = " UNIQUE";
    protected static String ON_DELETE_CASCADE = " ON DELETE CASCADE";
    protected static String ON_UPDATE_CASCADE = " ON UPDATE CASCADE";

    protected static final String FOREIGN_KEY_RELATION =
            " FOREIGN KEY(%s) REFERENCES %s(%s)";
    protected static final String PRIMARY_KEY_FORMAT = " PRIMARY KEY(%s)";
    protected String DELETE_TABLE =
            "DROP TABLE IF EXISTS " + getTableName();
    public static String PRIMARY_KEY = _ID;

    public abstract String getCreateTable();
    public abstract String getDeleteTable();
    public abstract String getTableName();
    public abstract String[] getColumns();
    public abstract String getPrimaryKey();
}
