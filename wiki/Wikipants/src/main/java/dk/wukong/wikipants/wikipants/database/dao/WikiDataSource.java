package dk.wukong.wikipants.wikipants.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Pair;
import android.widget.ArrayAdapter;

import java.lang.reflect.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import dk.wukong.wikipants.wikipants.database.contract.WikiEntry;
import dk.wukong.wikipants.wikipants.database.helper.PantsDBHelper;
import dk.wukong.wikipants.wikipants.models.Wiki;

/**
 * Created by mark on 4/17/14.
 *
 */
public class WikiDataSource
        extends BasePantsDataSource<WikiEntry> {
    protected static final String TAG = "WikiDataSource";

    public WikiDataSource(Context context) {
        super(new WikiEntry());
        dbHelper = new PantsDBHelper(context);
    }

    @Override
    public Wiki get(long id) throws SQLException {
        return (Wiki) super.get(id);
    }

    /**
     * Adds a Wiki to the database and returns the added Wiki.
     *
     * @param wiki a Wiki to be stored in the database.
     * @return a Wiki with the same identity as wiki,
     * if a new row was created this is reflected in this object.
     */
    public Wiki add(Wiki wiki) throws SQLException {
        return (Wiki) super.add(wiki);
    }

    public boolean remove(Wiki wiki) {
        return super.remove(wiki);
    }

    public Wiki update(Wiki wiki) {
        return (Wiki) super.update(wiki);
    }

    public ContentValues wikiToValues(Wiki wiki) {
        ContentValues contentValues = new ContentValues();
        Long id = wiki.getId();
        String title = wiki.getTitle();
        String subject = wiki.getSubject();
        String identifier = wiki.getIdentifier();

        contentValues.put(WikiEntry._ID, id);
        contentValues.put(WikiEntry.COLUMN_NAME_SUBJECT, subject);
        contentValues.put(WikiEntry.COLUMN_NAME_TITLE, title);
        contentValues.put(WikiEntry.COLUMN_NAME_IDENT, identifier);

        return contentValues;
    }

    @Override
    public ContentValues objectToValues(Object wiki) {
        return wikiToValues((Wiki) wiki);
    }

    @Override
    public Wiki cursorToObject(Cursor cursor) {
        Wiki wiki = new Wiki();
        int i = 0;

        wiki.setId(cursor.getLong(i));
        wiki.setSubject(cursor.getString(++i));
        wiki.setTitle(cursor.getString(++i));
        wiki.setIdentifier(cursor.getString(++i));

        return wiki;
    }

    public ArrayList<Wiki> getWikiListByRowValue(String row, String value) {
        String table = this.table.getTableName();
        String query;
        String[] columns;
        String[] argumentArray;
        Cursor cursor;
        ArrayList<Wiki> wikiSet = new ArrayList<Wiki>();

        query = String.format(
                "%s = ?",
                row
        );

        columns = this.table.getColumns();

        argumentArray = new String[]{
                value,
        };

        cursor = getCursor(table, query, columns, argumentArray);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            wikiSet.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        return wikiSet;
    }

    public ArrayList<Wiki> getWikiListBySubject(String subject) {
        return getWikiListByRowValue(WikiEntry.COLUMN_NAME_SUBJECT, subject);
    }

    public ArrayList<Wiki> getWikiListByTitle(String title) {
        return getWikiListByRowValue(WikiEntry.COLUMN_NAME_TITLE, title);
    }

    public Wiki getWikiByIdentifier(String identifier) {
        Wiki wiki;
        ArrayList<Wiki> wikiList =
                getWikiListByRowValue(WikiEntry.COLUMN_NAME_IDENT, identifier);

        if (wikiList.isEmpty() || wikiList.size() != 1) {
            wiki = null;
        } else {
            wiki = wikiList.get(0);
        }

        return wiki;
    }

    public ArrayList<Wiki> getWikis(){
        ArrayList<Wiki> wikis;
        Cursor cursor;

        cursor = database.query(WikiEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                null
        );

        cursor.moveToFirst();
        wikis = new ArrayList<Wiki>();
        while (!cursor.isAfterLast()) {
            wikis.add(cursorToObject(cursor));
            cursor.moveToNext();
        }

        return wikis;

    }

    public ArrayList<Pair<String, String>> getLocalWikiNameAndSubjectList() {
        ArrayList<Pair<String, String>> pairs;
        Cursor cursor;
        cursor = database.query(WikiEntry.TABLE_NAME,
                new String[]{
                        WikiEntry.COLUMN_NAME_TITLE,
                        WikiEntry.COLUMN_NAME_SUBJECT},
                null,
                null,
                null,
                null,
                null
        );
        cursor.moveToFirst();
        pairs = new ArrayList<Pair<String, String>>();
        while (!cursor.isAfterLast()) {
            pairs.add(
                    new Pair<String, String>(
                            cursor.getString(0),
                            cursor.getString(1))
            );
            cursor.moveToNext();
        }

        return pairs;
    }

    public Cursor getLocalWikiCursor() {
        Cursor cursor;
        cursor = database.rawQuery(
                String.format("SELECT * FROM %s ORDER BY %s",
                        WikiEntry.TABLE_NAME,
                        WikiEntry.COLUMN_NAME_TITLE),
                new String[] {});

        return cursor;
    }
}