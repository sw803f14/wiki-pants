package dk.wukong.wikipants.wikipants.database.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import dk.wukong.wikipants.wikipants.R;


import dk.wukong.wikipants.wikipants.database.contract.*;

/**
 * Created by mark on 4/21/14.
 *
 */
public class PantsDBHelper
        extends SQLiteOpenHelper{
    protected static String TAG = "BasePantsDBHelper";

    protected BasePantsTable[] tables = {
            new WikiEntry(),
            new ArticleEntry(),
            new ArchiveArticleEntry(),
            new TagEntry(),
            new TagListEntry(),
    };

    public BasePantsTable[] getTables() {
        return tables;
    }

    public PantsDBHelper(Context context) {
        super(
                context,
                context.getResources().getString(R.string.database_name),
                null,
                context.getResources().getInteger(R.integer.database_version)
        );
        // Log.d(TAG, "Created helper for table: " + table.getTableName());
    }

    private void makeTable(BasePantsTable table, SQLiteDatabase database){
        Log.d(TAG, String.format("Getting ready to create %s table.",
                table.getTableName()));
        Log.v(TAG, String.format("using\n%s", table.getCreateTable()));
        database.execSQL(table.getCreateTable());
        Log.d(TAG, String.format("Created %s table.", table.getTableName()));
    }

    public void dropTable(BasePantsTable table,
                          SQLiteDatabase database,
                          String message){
        Log.d(TAG, message);
        database.execSQL(table.getDeleteTable());
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        for(BasePantsTable table : tables){
            makeTable(table, database);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database,
                          int oldVersion,
                          int newVersion) {
        String message = "Dropping table %s because of upgrade.";
        for(BasePantsTable table : tables){
            dropTable(table,
                    database,
                    String.format(message, table.getTableName()));
        }
        onCreate(database);
    }
    @Override
    public void onOpen(SQLiteDatabase database) {
        super.onOpen(database);
        if (!database.isReadOnly()) {
            // Enable foreign key constraints
            database.execSQL("PRAGMA foreign_keys=ON;");
        }
    }
}
