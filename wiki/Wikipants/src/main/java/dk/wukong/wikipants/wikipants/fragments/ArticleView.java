package dk.wukong.wikipants.wikipants.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.webkit.WebView;
import android.webkit.WebViewFragment;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;

import dk.wukong.wikipants.wikipants.R;
import dk.wukong.wikipants.wikipants.web.WikiWebInterface;

public class ArticleView extends WebViewFragment {
    private static final String TAG = WebViewFragment.class.getSimpleName();

    public static final String WIKI = "wiki";
    public static final String ARTICLE = "article";

    private WikiWebInterface webInterface;
    private ArticleViewCallback callback;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ArticleView.
     */
    public static ArticleView newInstance(long wiki, long article) {
        ArticleView fragment = new ArticleView();
        Bundle args = new Bundle();
        args.putLong(WIKI, wiki);
        args.putLong(ARTICLE, article);
        fragment.setArguments(args);
        return fragment;
    }

    public ArticleView() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        long wiki, article;

        if (getArguments() != null) {
            wiki = getArguments().getLong(WIKI);
            article = getArguments().getLong(ARTICLE);
            webInterface = new WikiWebInterface(getActivity(), wiki, article);
        }

        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.article_view, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.action_create:
                callback.createArticle(webInterface.getWiki(), true);
                return true;
            case R.id.action_edit:
                callback.editArticle(webInterface.getArticle(), true);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart(){
        super.onStart();
        WebView webView = getWebView();

        webView.getSettings().setJavaScriptEnabled(true);
        webView.addJavascriptInterface(webInterface, "Wiki");

        // initial load, then we basically have handed off to webView/interface.
        //webView.loadUrl("file:///android_asset/wiki/main.html");
        webInterface.getCurrentPage();
        //;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callback = (ArticleViewCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ArticleViewCallback");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public interface ArticleViewCallback{
        public void createArticle(long wikiId, boolean backup);
        public void editArticle(long id, boolean backup);
    }
}
