package dk.wukong.wikipants.wikipants.models;

import android.content.ContentValues;
import android.os.Message;
import android.util.Log;
import android.webkit.WebView;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by mark on 3/31/14.
 * 
 */
public class Wiki {
    private static final String TAG = "wiki";

    private Long id;
    private String title;
    private String subject;
    private String identifier;

    public Wiki(){}

    public Wiki(String title, String subject){
        this(title, subject, null);
    }

    public Wiki(String title, String subject, Long id){
        this.title = title;
        this.subject = subject;
        this.id = id;
        this.identifier = generateIdent(title, subject);
    }

    public static String generateIdent(String title, String subject){
        MessageDigest digester;
        String ident;
        try{
            digester = MessageDigest.getInstance("MD5");
            digester.update(title.getBytes());
            digester.update(subject.getBytes());
            ident = new String(digester.digest());
        } catch(NoSuchAlgorithmException e){
            Log.e(TAG, e.getMessage());
            ident = "";
        }
        return ident;
    }

    public WebView generatePage(){
        return null;
    }

    public String getTitle() {
        return title;
    }

    public String getSubject() {
        return subject;
    }

    public String getIdentifier() {
        return identifier;
    }

    public Long getId() {
        return id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean equals(Object object){
        if(object == null){
            return false;
        } else if(object == this){
            return true;
        } else if(!(object instanceof Wiki)){
            return false;
        }

        Wiki wiki = (Wiki) object;
        boolean retval;
        if(getId() == null) {
            retval = getIdentifier().equals(wiki.getIdentifier()) &&
                    getSubject().equals(wiki.getSubject()) &&
                    getTitle().equals(wiki.getTitle());
        } else {
            retval = getId().equals(wiki.getId());
        }
        return retval;
    }
}
