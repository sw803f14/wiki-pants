package dk.wukong.wikipants.wikipants.models;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Created by mark on 3/31/14.
 *
 */
public class Article {
    private static final String TAG = "Article";

    private Long id;
    private String title;
    private String body;
    private ArrayList<String> tags = new ArrayList<String>();
    private Long wiki;
    private long modified;

    public Article(){
        this("", "", 0, new String[]{});
    }

    public Article(String title, String body, long modified){
        this(title, body, modified, new String[]{});
    }

    public Article(String title, String body, long modified, String[] tags){
        this.title = title;
        this.body = body;
        this.modified = modified;
        this.setTags(tags);
    }

    public Article(Long id, String title,
                   String body, String tags,
                   long modified){
        this.id = id;
        this.title = title;
        this.body = body;
        setTags(tags);
        this.modified = modified;
        this.wiki = null;

    }

    public void setTitle(String title){
        this.title = title;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setWiki(Long wiki) {
        this.wiki = wiki;
    }
    public void setWiki(Wiki wiki) {
        this.wiki = wiki.getId();
    }

    public void setModified(long modified) {
        this.modified = modified;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setTags(List<String> tags) {
        this.tags = new ArrayList<String>(tags);
    }

    public void setTags(String[] tags) {
        this.setTags(Arrays.asList(tags));
    }

    /**
     *
     * @param tags a string containing a comma separated list of tags.
     */
    public void setTags(String tags) {
        this.setTags(tags.split(","));
    }

    public void addTag(String tag){
        this.tags.add(tag);
    }

    public Long getId() {
        return id;
    }

    public ArrayList<String> getTags(){
        return this.tags;
    }

    public String getTitle(){
        return this.title;
    }

    public String getBody(){
        return this.body;
    }

    public Long getWiki() {
        return wiki;
    }

    public long getModified() {
        return this.modified;
    }

    @Override
    public boolean equals(Object object){
        if(object == null){
            return false;
        } else if(object == this){
            return true;
        } else if(!(object instanceof Article)){
            return false;
        }

        Article article = (Article) object;
        boolean retval = true;

        if(this.wiki != null && article.getWiki() != null){
            retval = this.wiki.equals(article.getWiki());
        }

        if(this.id == null || article.getId() == null){
            retval = retval && title.equals(article.getTitle());
        } else {
            retval = retval && id.equals(article.getId());
        }

        return retval;
    }

    public String getTagsAsCommaString() {
        String string = "";

        for(int i = 0; i < tags.size(); ++i){
            string += tags.get(i);
            if(i != tags.size()-1){
                string += ", ";
            }
        }

        return string;
    }
}
