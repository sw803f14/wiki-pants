package dk.wukong.wikipants.wikipants.database.contract;

/**
 * Created by mark on 4/22/14.
 */
public class ArticleEntry extends BasePantsTable {
    // Defining what an article entry looks like.
    public static String TABLE_NAME = "article";
    public static String COLUMN_NAME_TITLE = "title";
    public static String COLUMN_NAME_BODY = "body";
    public static String COLUMN_NAME_MODIFIED = "modified";
    public static String COLUMN_NAME_WIKI = "wiki";

    private static String[] COLUMNS = {
            _ID,
            COLUMN_NAME_TITLE,
            COLUMN_NAME_BODY,
            COLUMN_NAME_MODIFIED,
            COLUMN_NAME_WIKI
    };
    private static String WIKI_FOREIGN =
            String.format(FOREIGN_KEY_RELATION,
                    COLUMN_NAME_WIKI,
                    WikiEntry.TABLE_NAME,
                    WikiEntry.PRIMARY_KEY) +
                    ON_DELETE_CASCADE;
    private static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
            COLUMN_NAME_TITLE + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            COLUMN_NAME_BODY + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_MODIFIED + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            COLUMN_NAME_WIKI + INT_TYPE + NOT_NULL + COMMA_SEP +
            WIKI_FOREIGN +
            ");";

    @Override
    public String getCreateTable() {
        return CREATE_TABLE;
    }

    @Override
    public String getDeleteTable() {
        return DELETE_TABLE;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    public String getPrimaryKey() {
        return PRIMARY_KEY;
    }
}
