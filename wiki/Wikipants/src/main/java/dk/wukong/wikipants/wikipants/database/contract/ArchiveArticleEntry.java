package dk.wukong.wikipants.wikipants.database.contract;

/**
 * Created by mark on 4/22/14.
 *
 */
public class ArchiveArticleEntry extends BasePantsTable {
    public static String TABLE_NAME = "archive_article";
    public static String COLUMN_NAME_BODY = "body";
    public static String COLUMN_NAME_MODIFIED = "modified";
    public static String COLUMN_NAME_ARTICLE = "article";

    private static String[] COLUMNS = {
            _ID,
            COLUMN_NAME_BODY,
            COLUMN_NAME_MODIFIED,
            COLUMN_NAME_ARTICLE
    };
    private static String ARTICLE_FOREIGN =
            String.format(FOREIGN_KEY_RELATION,
                    COLUMN_NAME_ARTICLE,
                    ArticleEntry.TABLE_NAME,
                    ArticleEntry.PRIMARY_KEY);
    private static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
            COLUMN_NAME_ARTICLE + NOT_NULL + COMMA_SEP +
            COLUMN_NAME_BODY + TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_MODIFIED + TEXT_TYPE + NOT_NULL + COMMA_SEP +
            ARTICLE_FOREIGN +
            ");";

    @Override
    public String getCreateTable() {
        return CREATE_TABLE;
    }

    @Override
    public String getDeleteTable() {
        return DELETE_TABLE;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    public String getPrimaryKey() {
        return PRIMARY_KEY;
    }
}