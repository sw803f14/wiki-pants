package dk.wukong.wikipants.wikipants.views;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import dk.wukong.wikipants.wikipants.R;
import dk.wukong.wikipants.wikipants.database.contract.WikiEntry;

/**
* Created by mark on 5/16/14.
*/
public class WikiCursorAdapter extends CursorAdapter {
    // holding the cursor and inflater may or may not be a great idea.l
    LayoutInflater inflater;
    Cursor cursor;

    // view holder pattern, avoiding that something disused is in memory.
    static class ViewHolder{
        long id;
        TextView title;
        TextView subject;
    }

    public WikiCursorAdapter(Context context, Cursor c, int flags){
        super(context, c, flags);
        this.inflater = LayoutInflater.from(context);
        this.cursor = c;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View convertView = inflater.inflate(R.layout.wiki_row_layout, null);
        ViewHolder holder = new ViewHolder();

        holder.title = (TextView)
                convertView.findViewById(R.id.wikiRowTitle);
        holder.subject = (TextView)
                convertView.findViewById(R.id.wikiRowSubject);

        convertView.setTag(holder);

        return convertView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        if(holder == null){
            holder = new ViewHolder();
            view.setTag(holder);
        }

        holder.title.setText(this.cursor.getString(
                this.cursor.getColumnIndex(
                        WikiEntry.COLUMN_NAME_TITLE)));

        holder.subject.setText(
                this.cursor.getString(this.cursor.getColumnIndex(
                        WikiEntry.COLUMN_NAME_SUBJECT)));
    }
}
