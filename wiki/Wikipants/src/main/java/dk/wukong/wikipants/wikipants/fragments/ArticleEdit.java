package dk.wukong.wikipants.wikipants.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

import dk.wukong.wikipants.wikipants.R;
import dk.wukong.wikipants.wikipants.database.dao.ArticleDataSource;
import dk.wukong.wikipants.wikipants.models.Article;
import dk.wukong.wikipants.wikipants.util.ArticleFileManager;

public class ArticleEdit extends Fragment {
    private static final String TAG = ArticleEdit.class.getSimpleName();

    private static final String ID = "id";

    private long id;

    private ArticleEditCallback callback;
    private ArticleFileManager articleFileMan;

    public static Fragment newInstance(long id) {
        ArticleEdit fragment = new ArticleEdit();
        Bundle args = new Bundle();
        args.putLong(ID, id);
        fragment.setArguments(args);
        return fragment;
    }

    public ArticleEdit() {
        // Required empty public constructor
    }

    public void submit(){
        View view;
        ArticleDataSource dataSource;
        Article article;
        String title, body, tags;

        view = getView();

        title = ((TextView) view.findViewById(R.id.title))
                .getText().toString();

        body = ((TextView) view.findViewById(R.id.body))
                .getText().toString();

        tags = ((TextView) view.findViewById(R.id.tags))
                .getText().toString();

        article = new Article(
                id,
                title,
                "", //body is not stored in the DB any more.
                tags,
                System.currentTimeMillis());

        dataSource = new ArticleDataSource(getActivity());

        try {
            dataSource.open();
            article = dataSource.update(article);
            dataSource.close();
        } catch (SQLException e) {
            Log.e(TAG, "Could not open database connection.");
        }

        articleFileMan.update(article.getWiki(), article.getId(),
                body, "markdown");

        callback.viewArticle(article.getWiki(), article.getId(), false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_submit){
            submit();
            return true;
        }

        return true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            id = getArguments().getLong(ID, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article_create,
                container,
                false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callback = (ArticleEditCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ArticleCreateCallback");
        }

        articleFileMan = new ArticleFileManager(activity);
    }

    public void populateFields(){
        View view;
        TextView title, body, tags;
        ArticleDataSource dataSource;
        Article article;
        File file;

        view = getView();
        title = (TextView) view.findViewById(R.id.title);
        body = (TextView) view.findViewById(R.id.body);
        tags = (TextView) view.findViewById(R.id.tags);

        try{
            dataSource = new ArticleDataSource(getActivity());
            dataSource.open();
            article = dataSource.get(id);
            dataSource.close();
        } catch (SQLException e){
            article = new Article();
        }

        title.setText(article.getTitle());
        tags.setText(article.getTagsAsCommaString());

        file = articleFileMan.get(article.getWiki(), article.getId());
        if(!file.exists()){
            body.setText("");
        } else {
            try {
                BufferedReader reader =
                        new BufferedReader(new FileReader(file));
                String read;

                while((read = reader.readLine()) != null){
                    body.append(read);
                    body.append("\n");
                }

            } catch (IOException e) {
                Log.e(TAG, e.getMessage());
            }
        }

    }

    @Override
    public void onStart(){
        super.onStart();
        populateFields();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public interface ArticleEditCallback {
        public void viewArticle(long wiki, long article, boolean backup);
    }
}
