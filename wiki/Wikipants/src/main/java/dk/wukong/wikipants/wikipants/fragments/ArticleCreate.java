package dk.wukong.wikipants.wikipants.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.sql.SQLException;

import dk.wukong.wikipants.wikipants.R;
import dk.wukong.wikipants.wikipants.database.dao.ArticleDataSource;
import dk.wukong.wikipants.wikipants.models.Article;
import dk.wukong.wikipants.wikipants.util.ArticleFileManager;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link dk.wukong.wikipants.wikipants.fragments.ArticleCreate.ArticleCreateCallback} interface
 * to handle interaction events.
 * Use the {@link ArticleCreate#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class ArticleCreate extends Fragment {
    private static final String TAG = ArticleCreate.class.getSimpleName();

    private static final String TITLE = "title";
    private static final String WIKI = "wiki";

    private long wiki;
    private String title;

    private ArticleFileManager articleFileMan;
    private ArticleCreateCallback callback;

    public static ArticleCreate newInstance(long wiki, String title) {
        ArticleCreate fragment = new ArticleCreate();
        Bundle args = new Bundle();
        args.putLong(WIKI, wiki);
        args.putString(TITLE, title);
        fragment.setArguments(args);
        return fragment;
    }

    public static ArticleCreate newInstance(long wiki) {
        ArticleCreate fragment = new ArticleCreate();
        Bundle args = new Bundle();
        args.putLong(WIKI, wiki);
        fragment.setArguments(args);

        return fragment;
    }

    public ArticleCreate() {
        // Required empty public constructor
    }

    public void submit(){
        View view;
        ArticleDataSource dataSource;
        Article article;
        String title, body, tags;

        view = getView();

        title = ((TextView) view.findViewById(R.id.title))
                .getText().toString();

        body = ((TextView) view.findViewById(R.id.body))
                .getText().toString();

        tags = ((TextView) view.findViewById(R.id.tags))
                .getText().toString();

        article = new Article();

        article.setTitle(title);
        article.setWiki(wiki);
        article.setTags(tags);
        article.setModified(System.currentTimeMillis());

        dataSource = new ArticleDataSource(getActivity());

        try {
            dataSource.open();
            article = dataSource.add(article);
            dataSource.close();
        } catch (SQLException e) {
            Log.e(TAG, "Could not open database connection.");
        }

        articleFileMan.create(wiki, article.getId(), body, "markdown");
        callback.viewArticle(wiki, article.getId(), false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            title = getArguments().getString(TITLE, "");
            wiki = getArguments().getLong(WIKI, 0);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_article_create,
                container,
                false);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.create, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.action_submit){
            submit();
            return true;
        }

        return true;
    }

    @Override
    public void onStart(){
        super.onStart();
        populateTitle();
    }

    public void populateTitle(){
        if(!title.equals("")){
            ((TextView)getView().findViewById(R.id.title)).setText(title);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            callback = (ArticleCreateCallback) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ArticleCreateCallback");
        }
        articleFileMan = new ArticleFileManager(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        callback = null;
    }

    public interface ArticleCreateCallback {
        public void viewArticle(long wiki, long article, boolean backup);
    }
}
