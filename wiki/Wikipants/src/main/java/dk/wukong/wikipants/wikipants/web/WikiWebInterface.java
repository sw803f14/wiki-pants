package dk.wukong.wikipants.wikipants.web;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.webkit.JavascriptInterface;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

import dk.wukong.wikipants.wikipants.database.contract.ArticleEntry;
import dk.wukong.wikipants.wikipants.database.dao.ArticleDataSource;
import dk.wukong.wikipants.wikipants.models.Article;
import dk.wukong.wikipants.wikipants.util.ArticleFileManager;

/**
 * Created by mark on 5/13/14.
 *
 */
public class WikiWebInterface {
    private static final String TAG = WikiWebInterface.class.getSimpleName();

    private Context context;
    private ArticleFileManager articleFileManager;
    private ArticleDataSource dataSource;
    private WebCallback callback;
    private long article;
    private long wiki;

    public WikiWebInterface(Context context, long wiki, long article) {
        this.context = context;

        try {
            callback = (WebCallback) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement WebCallback");
        }

        this.article = article;
        this.wiki = wiki;
        this.dataSource = new ArticleDataSource(context);
        this.articleFileManager = new ArticleFileManager(context);
    }


    public void setArticle(long article){
        this.article = article;
    }

    public void setWiki(long wiki){
        this.wiki = wiki;
    }


    public long getArticle(){
        return article;
    }

    public long getWiki(){
        return wiki;
    }

    public void getCurrentPage(){
        getPage(wiki, article);
    }

    @JavascriptInterface
    public void getPage(long wiki, long article){
        String location;
        File html, md;
        html = articleFileManager.get(wiki, article, "html");
        md = articleFileManager.get(wiki, article, "markdown");
        this.wiki = wiki;
        this.article = article;

        if(!html.exists() || html.lastModified() < md.lastModified()){
            location = "file:///android_asset/wiki/main.html";
        } else {
            location = html.getAbsolutePath();
        }

        location = "file:///android_asset/wiki/main.html";

        callback.getPage(location);
    }

    @JavascriptInterface
    public String requestBody(){
        String body;

        body = "";
        try {
            File file = articleFileManager.get(wiki, article, "markdown");
            BufferedReader reader =
                    new BufferedReader(new FileReader(file));
            String read;

            while((read = reader.readLine()) != null){
                body += read;
                body += "\n";
            }
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        }

        return body;
    }

    @JavascriptInterface
    public String getReference(String name){
        Article article;
        String page = "";
        try{
            dataSource.open();
            article = dataSource.get(wiki, name);

            if(article != null){
                page = "javascript:Wiki.getPage(%d, %d)";
                page = String.format(page, wiki, article.getId());
            } else {
                page = "javascript:Wiki.createPage(%d, '%s')";
                page = String.format(page, wiki, name);
            }

            dataSource.close();
        } catch(SQLException e){
            Log.e(TAG, e.getMessage());
        }
        return page;
    }

    @JavascriptInterface
    public void createPage(long wiki, String title){
        // since creating a page from a title is like editing an empty
        // article that is what this does.
        Article article;
        article = new Article();
        article.setWiki(wiki);
        article.setTitle(title);
        article.setBody("");
        article.setModified(System.currentTimeMillis());

        try{
            dataSource.open();
            article = dataSource.add(article);
            dataSource.close();
        } catch (SQLException e){
            Log.e(TAG, e.getMessage());
        }

        callback.editPage(article.getId());
    }

    @JavascriptInterface
    public void saveHTML(String body){
        articleFileManager.create(wiki,article,body,"html");
    }

    public interface WebCallback{
        abstract void editPage(long article);
        abstract void getPage(String location);
    }
}
