package dk.wukong.wikipants.wikipants.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.webkit.ValueCallback;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

import dk.wukong.wikipants.wikipants.R;
import dk.wukong.wikipants.wikipants.database.contract.ArticleEntry;
import dk.wukong.wikipants.wikipants.database.dao.ArticleDataSource;
import dk.wukong.wikipants.wikipants.database.dao.WikiDataSource;
import dk.wukong.wikipants.wikipants.fragments.ArticleCreate;
import dk.wukong.wikipants.wikipants.fragments.ArticleEdit;
import dk.wukong.wikipants.wikipants.fragments.ArticleView;
import dk.wukong.wikipants.wikipants.fragments.WikiList;
import dk.wukong.wikipants.wikipants.fragments.WikiCreate;
import dk.wukong.wikipants.wikipants.models.Article;
import dk.wukong.wikipants.wikipants.util.ArticleFileManager;
import dk.wukong.wikipants.wikipants.web.WikiWebInterface;

public class MainActivity extends Activity implements
        WikiList.WikiListCallback,
        WikiCreate.WikiCreateCallback,
        ArticleView.ArticleViewCallback,
        ArticleCreate.ArticleCreateCallback,
        ArticleEdit.ArticleEditCallback,
        WikiWebInterface.WebCallback {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, new WikiList())
                    .commit();
        }
    }

    public void fragmentSwap(Fragment fragment, boolean backup){
        FragmentTransaction transaction;

        transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void viewWiki(long wikiId, boolean backup){
        viewWikiArticle(wikiId, backup, "main");
    }

    public void viewWikiArticle(long wikiId, boolean backup, String name){
        long id = 0;
        ArticleDataSource dataSource;
        dataSource = new ArticleDataSource(this);

        try{
            dataSource.open();
            Cursor cursor = dataSource.getArticleByTitleAndWiki(
                    name,
                    wikiId,
                    new String[]{ArticleEntry._ID});
            if(!cursor.isAfterLast()){
                cursor.moveToFirst();
                id = cursor.getLong(0);
            }
            dataSource.close();
        } catch (SQLException e){
            Log.e(TAG, e.getMessage());
        }

        if(id == 0){
            fragmentSwap(ArticleCreate.newInstance(wikiId, name), backup);
        } else {
            fragmentSwap(ArticleView.newInstance(wikiId, id), backup);
        }
    }

    @Override
    public void editPage(long article) {
        editArticle(article, false);
    }

    @Override
    public void getPage(String location) {
        ArticleView view;

        view = (ArticleView)
                getFragmentManager().findFragmentById(R.id.container);

        view.getWebView().loadUrl(location);
    }

    @Override
    public void createWiki(boolean backup){
        fragmentSwap(new WikiCreate(), backup);
    }

    @Override
    public void editArticle(long article, boolean backup) {
        fragmentSwap(ArticleEdit.newInstance(article), backup);
    }

    @Override
    public void createArticle(long wikiId, boolean backup){
        fragmentSwap(ArticleCreate.newInstance(wikiId), backup);
    }

    @Override
    public void viewArticle(long wiki, long article, boolean backup){
        fragmentSwap(ArticleView.newInstance(wiki, article), backup);
    }
}
