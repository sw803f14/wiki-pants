package dk.wukong.wikipants.wikipants.database.contract;

/**
 * Created by mark on 4/22/14.
 *
 */
public class TagEntry extends BasePantsTable {
    public static String TABLE_NAME = "tag";
    public static String COLUMN_NAME_TAG = "tag";

    private static String[] COLUMNS = {_ID, COLUMN_NAME_TAG};
    private static String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME +
            " (" +
            _ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + COMMA_SEP +
            COLUMN_NAME_TAG +
            ");";

    @Override
    public String getCreateTable() {
        return CREATE_TABLE;
    }

    @Override
    public String getDeleteTable() {
        return DELETE_TABLE;
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String[] getColumns() {
        return COLUMNS;
    }

    @Override
    public String getPrimaryKey() {
        return PRIMARY_KEY;
    }
}
